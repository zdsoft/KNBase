#
# Be sure to run `pod lib lint KNBase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KNBase'
  s.version          = '0.7.4'
  s.summary          = '基础库.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
基础库文件
                       DESC

  s.homepage         = 'https://gitee.com/zdsoft/KNBase'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vbn' => 'zhoucan123@vip.qq.com' }
  s.source           = { :git => 'https://gitee.com/zdsoft/KNBase.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'KNBase/Classes/**/*'
  
  s.resource_bundles = {
    'KNBase' => ['KNBase/Assets/*']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'AFNetworking','3.2.1'
  s.dependency 'FDFullscreenPopGesture', '1.1'
  s.dependency 'MJRefresh','3.1.15.3'
  s.dependency 'Masonry','1.1.0'
  s.dependency 'YYCache','1.0.4'
  s.dependency 'HWWeakTimer','1.0'
  s.dependency 'MBProgressHUD','1.1.0'
  
  
end
