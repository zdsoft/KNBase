//
//  NoneViewController.m
//  KNBase
//
//  Created by vbn on 05/24/2018.
//  Copyright (c) 2018 vbn. All rights reserved.
//

#import "NoneViewController.h"
#import "UIView+KNToast.h"
#import "KNHelperFunctions.h"
#import "NSBundle+KNBase.h"
#import "KNMacroDefines.h"
#import "KNHelperFunctions.h"
#import "NoneListViewController.h"
@interface NoneViewController ()

@end

@implementation NoneViewController

-(BOOL)needBackItem {
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.view makeToast:@"ssss"];
    NSBundle *podBundle = [NSBundle KNBaseBundle];
    UIImage *backgroundImage = [UIImage imageNamed:@"kn_nav_back" inBundle:podBundle compatibleWithTraitCollection:nil];
        NSLog(@"%@",backgroundImage);
    self.title = @"sss";
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NoneListViewController *s = [[NoneListViewController alloc] init];
        s.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:s animated:YES];
    });
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
