//
//  NoneAppDelegate.h
//  KNBase
//
//  Created by vbn on 05/24/2018.
//  Copyright (c) 2018 vbn. All rights reserved.
//

@import UIKit;

@interface NoneAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
