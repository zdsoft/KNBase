//
//  main.m
//  KNBase
//
//  Created by vbn on 05/24/2018.
//  Copyright (c) 2018 vbn. All rights reserved.
//

@import UIKit;
#import "NoneAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NoneAppDelegate class]));
    }
}
