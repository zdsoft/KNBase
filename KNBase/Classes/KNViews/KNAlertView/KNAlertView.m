
//  Copyright (c) 2013 Fenbi. All rights reserved.
//

#import "KNAlertView.h"

@interface KNAlertView () <UIAlertViewDelegate>

@property (copy, nonatomic) void (^clickedBlock)(KNAlertView *, BOOL, NSInteger);

@end

@implementation KNAlertView

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
       clickedBlock:(void (^)(KNAlertView *alertView, BOOL cancelled, NSInteger buttonIndex))clickedBlock
  cancelButtonTitle:(NSString *)cancelButtonTitle
  otherButtonTitles:(NSString *)otherButtonTitles, ...
{

    self = [self initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil ];
    if (self) {
        _clickedBlock = clickedBlock;
        va_list _arguments;
        va_start(_arguments, otherButtonTitles);
        for (NSString *key = otherButtonTitles; key != nil; key = (__bridge NSString *)va_arg(_arguments, void *)) {
            [self addButtonWithTitle:key];
        }
        va_end(_arguments);
    }
    return self;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(_clickedBlock){
         _clickedBlock(self, buttonIndex==self.cancelButtonIndex, buttonIndex);
    }
}

@end
