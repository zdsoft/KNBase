//
//  NSDictionary+UrlEncoding.m
//  Backhome
//
//  Created by vbn on 2018/4/11.
//  Copyright © 2018年 SmartHome. All rights reserved.
//

#import "NSDictionary+KNUrlEncoding.h"

@implementation NSDictionary (UrlEncoding)

// helper function: get the string form of any object
static NSString *KNToString(id object) {
    return [NSString stringWithFormat: @"%@", object];
}

// helper function: get the url encoded string form of any object
static NSString *KNUrlEncode(id object) {
    NSString *string = KNToString(object);
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}

-(NSString*)KNUrlEncodedString {
    NSMutableArray *parts = [NSMutableArray array];
    for (id key in self) {
        id value = [self objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@=%@", KNUrlEncode(key), KNUrlEncode(value)];
        [parts addObject: part];
    }
    return [parts componentsJoinedByString: @"&"];
}


@end
