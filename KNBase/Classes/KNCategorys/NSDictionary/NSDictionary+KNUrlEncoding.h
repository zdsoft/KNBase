//
//  NSDictionary+UrlEncoding.h
//  Backhome
//
//  Created by vbn on 2018/4/11.
//  Copyright © 2018年 SmartHome. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (KNUrlEncoding)

-(NSString*)KNUrlEncodedString;

@end
