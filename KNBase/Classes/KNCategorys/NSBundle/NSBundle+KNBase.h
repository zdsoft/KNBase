//
//  NSBundle+KNBase.h
//  KNBase
//
//  Created by vbn on 2018/5/24.
//

#import <Foundation/Foundation.h>

@interface NSBundle (KNBase)

+ (NSBundle *)KNBaseBundle;

@end
