//
//  NSBundle+KNBase.m
//  KNBase
//
//  Created by vbn on 2018/5/24.
//

#import "NSBundle+KNBase.h"
#import "KNArchiveModel.h"

@implementation NSBundle (KNBase)

+ (NSBundle *)KNBaseBundle {
    return [self bundleWithURL:[self my_myLibraryBundleURL]];
}

+ (NSURL *)my_myLibraryBundleURL {
    NSBundle *bundle = [NSBundle bundleForClass:[KNArchiveModel class]];
    return [bundle URLForResource:@"KNBase" withExtension:@"bundle"];
}

@end
